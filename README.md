# HashiCorp Demo

## Assumptions

* `AWS_ACCESS_KEY_ID` && `AWS_SECRET_ACCESS_KEY` **OR** `AWS_PROFILE` are exported.
* You have make, cURL, terraform (>= 0.10.3), packer (>= 1.1.2) installed
* Region is set by default to `us-east-1` but can be changed via parameters (`export TF_VAR_region=<region>`). However, I haven't tested with a different region.

## Packer

First, build the image that will be used when building the infrastructure via TF:

```sh
cd packer
packer build base-docker.json
```

## Terraform

### Pre-requisites

Export the TF variable that points to your public key (for SSH access):

```sh
export TF_VAR_public_key_path=/path/to/your/public/key.pub
```

Inbound access (via SSH/web) is allowed from 0.0.0.0/0 by default. If you want to limit it:

```sh
export TF_VAR_inbound_from=x.x.x.x/yy
```

### Bring up the infrastructure

To create the infrastructure for Consul, Vault, etc:

```sh
cd terraform/infra
make apply
```

You should be able to open the `haproxy_www_demo_url` displayed and reload a few times to see HAproxy load balancing in action.

### Vault

To initialize vault:

```sh
make vault-init
```

You'll then need to run `make vault-unseal` 3 times with the keys displayed by the previous command.

### Destroy

Bring it all down with:

```sh
make destroy
```

## Notes

* This was produced purely as a PoC and not with best practices in mind, so shortcuts were taken (e.g. no TLS used anywhere, volume mounted on the Nomad server to the HAproxy container, etc).

## Author

Gonzalo Servat <gservat@gmail.com>
