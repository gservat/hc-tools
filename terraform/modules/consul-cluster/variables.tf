variable "vpc_id" {}

variable "subnet_ids" {
  type = "list"
}

variable "inbound_from" {}

variable "instance_type" {
  default = "t2.micro"
}

variable "num_nodes" {}
variable "region" {}

variable "extra_sgs" {
  type    = "list"
  default = []
}

variable "key_name" {}
