terraform {
  required_version = ">= 0.10.3"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  owners = ["self"]

  filter {
    name   = "name"
    values = ["base-docker-*"]
  }
}

data "template_file" "bootstrap" {
  template = "${file("${path.module}/files/bootstrap.sh")}"

  vars {
    region    = "${var.region}"
    num_nodes = "${var.num_nodes}"
  }
}

resource "aws_launch_configuration" "main" {
  name_prefix          = "consul-node-"
  image_id             = "${data.aws_ami.ubuntu.image_id}"
  instance_type        = "${var.instance_type}"
  user_data            = "${data.template_file.bootstrap.rendered}"
  iam_instance_profile = "${aws_iam_instance_profile.consul-instance-profile.id}"

  security_groups = [
    "${aws_security_group.consul-nodes.id}",
    "${var.extra_sgs}",
  ]

  lifecycle {
    create_before_destroy = true
  }

  key_name = "${var.key_name}"
}

resource "aws_autoscaling_group" "consul-asg" {
  name                 = "consul-nodes"
  launch_configuration = "${aws_launch_configuration.main.name}"
  min_size             = "${var.num_nodes}"
  max_size             = "${var.num_nodes}"
  vpc_zone_identifier  = ["${var.subnet_ids}"]
  target_group_arns    = ["${aws_lb_target_group.main.arn}"]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "Consul Node"
    propagate_at_launch = true
  }

  tag {
    key                 = "Purpose"
    value               = "Consul-Node"
    propagate_at_launch = true
  }
}

resource "aws_lb" "main" {
  name     = "consul-lb"
  internal = false
  subnets  = ["${var.subnet_ids}"]

  security_groups = [
    "${aws_security_group.consul-lb.id}",
  ]

  tags {
    Name = "Consul ALB"
  }
}

# TODO - TLS only
resource "aws_lb_listener" "http" {
  load_balancer_arn = "${aws_lb.main.arn}"
  port              = "8500"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.main.arn}"
    type             = "forward"
  }
}

resource "aws_lb_target_group" "main" {
  name     = "consul-lb-tg"
  port     = 8500
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 5
    timeout             = 5
    path                = "/ui/"
    interval            = 30
    matcher             = 200
  }
}
