resource "aws_iam_policy" "consul-autojoin" {
  name        = "vault-consul-autojoin"
  path        = "/"
  description = "This policy allows a consul agent to find other consul nodes"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
    EOF
}

resource "aws_iam_role" "vault-node-role" {
  name = "vault-node-role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "consul-autojoin" {
  name       = "consul-discovery"
  roles      = ["${aws_iam_role.vault-node-role.name}"]
  policy_arn = "${aws_iam_policy.consul-autojoin.arn}"
}

resource "aws_iam_instance_profile" "main" {
  name = "vault-instance-profile"
  role = "${aws_iam_role.vault-node-role.name}"
}
