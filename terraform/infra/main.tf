module "consul-cluster" {
  source       = "../modules/consul-cluster"
  vpc_id       = "${aws_vpc.main.id}"
  subnet_ids   = "${aws_subnet.public.*.id}"
  num_nodes    = 3
  region       = "${var.region}"
  inbound_from = "${var.inbound_from}"
  extra_sgs    = ["${aws_security_group.common.id}"]
  key_name     = "${aws_key_pair.main.key_name}"
}

module "vault-server" {
  source       = "../modules/vault-instance"
  vpc_id       = "${aws_vpc.main.id}"
  subnet_id    = "${aws_subnet.public.0.id}"
  region       = "${var.region}"
  inbound_from = "${var.inbound_from}"
  extra_sgs    = ["${aws_security_group.common.id}"]
  key_name     = "${aws_key_pair.main.key_name}"
}

# Nomad AiO = All In One
module "nomad-demo" {
  source       = "../modules/nomad-aio"
  vpc_id       = "${aws_vpc.main.id}"
  subnet_id    = "${aws_subnet.public.0.id}"
  region       = "${var.region}"
  inbound_from = "${var.inbound_from}"
  extra_sgs    = ["${aws_security_group.common.id}"]
  key_name     = "${aws_key_pair.main.key_name}"
}
