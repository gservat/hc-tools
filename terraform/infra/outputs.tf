output "consul_url" {
  value = "${module.consul-cluster.url}"
}

output "haproxy_www_demo_url" {
  value = "http://${module.nomad-demo.nomad-external-ip}/"
}
